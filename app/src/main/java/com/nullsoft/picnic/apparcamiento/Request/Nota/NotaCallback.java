package com.nullsoft.picnic.apparcamiento.Request.Nota;


/**
 * Created by ruben on 02/02/17.
 */

public interface NotaCallback {

    void onSuccess(DTO_Nota result);

    void onFail(String result);

}
