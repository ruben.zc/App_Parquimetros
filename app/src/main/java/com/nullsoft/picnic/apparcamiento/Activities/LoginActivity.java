package com.nullsoft.picnic.apparcamiento.Activities;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewAnimationUtils;
import android.view.Window;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.nullsoft.picnic.apparcamiento.Adapters.Adaptador;
import com.nullsoft.picnic.apparcamiento.DTO_Estacionamiento;
import com.nullsoft.picnic.apparcamiento.Helpers.Criptografo;
import com.nullsoft.picnic.apparcamiento.Helpers.GPSTracker;
import com.nullsoft.picnic.apparcamiento.R;
import com.nullsoft.picnic.apparcamiento.Request.Nota.CommunicatorNotas;
import com.nullsoft.picnic.apparcamiento.Request.Nota.DTO_Nota;
import com.nullsoft.picnic.apparcamiento.Request.Nota.NotaCallback;
import com.polyak.iconswitch.IconSwitch;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import cn.pedant.SweetAlert.SweetAlertDialog;



public class LoginActivity extends AppCompatActivity implements OnMapReadyCallback,
        IconSwitch.CheckedChangeListener, ValueAnimator.AnimatorUpdateListener,
        View.OnClickListener, NotaCallback {

    private static final int REQUEST_READ_CONTACTS = 0;

    //private static final int DURATION_COLOR_CHANGE_MS = 400;
    private static final String TAG = "LoginActivity";
    private static final int REQUEST_CODE_CAMARA = 1000;

    private int[] toolbarColors;
    private int[] statusBarColors;
    private ValueAnimator statusBarAnimator;
    private Interpolator contentInInterpolator;
    private Interpolator contentOutInterpolator;
    private Point revealCenter;

    private Window window;
    private View toolbar;
    private View content;
    private IconSwitch iconSwitch;

    public TextView title;

    final CommunicatorNotas communicator = new CommunicatorNotas();


    final Gson gson = new Gson();
    public Criptografo criptografo = new Criptografo("PQws");

    DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
    DatabaseReference mensajeRef = ref.child("lstEstacionamientos");

    //mapa
    MapView mMapView;
    private GoogleMap googleMap;
    double latitude;
    double longitude;


    public List<DTO_Estacionamiento> lst;

    public  FloatingActionButton fab;
    public FloatingActionButton fab2;




    //variables modal
    public LinearLayout modalDireccion;
    public int tamanito = 0;
    public boolean bPantallaInicio = true;
    private static final int DURATION_COLOR_CHANGE_MS = 180;
    public int targetTranslation = 0;
    public Interpolator interpolator = null;

    public SweetAlertDialog pDialog ;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.





















        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);

        fab2 = (FloatingActionButton)findViewById(R.id.btn_ticket);

        fab2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AbrirModal();
            }
        });

        fab = (FloatingActionButton)findViewById(R.id.btn_crono);
        fab.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(LoginActivity.this, FotoActivity.class);
                startActivityForResult(intent,1000);
            }
        });


        ImageView btn_Cerrar = (ImageView)findViewById(R.id.btn_Cerrar);
        btn_Cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CerrarModal();
            }
        });

        modalDireccion = (LinearLayout)findViewById(R.id.modalNota);
        modalDireccion.setVisibility(View.GONE);

        window = getWindow();

        initColors();
        initAnimationRelatedFields();

        content = findViewById(R.id.content);
        toolbar = findViewById(R.id.toolbar);

        title = (TextView) findViewById(R.id.toolbar_title);
        title.setText("Parquimetros");
        title.setTextColor(getResources().getColor(R.color.colorAccent));

        iconSwitch = (IconSwitch) findViewById(R.id.icon_switch);
        iconSwitch.setCheckedChangeListener(this);
        updateColors(false);

        InicializarMapa(savedInstanceState,this);


        mensajeRef.addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {



                        List<DTO_Estacionamiento> lst = new ArrayList<>();
                        for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                            // TODO: handle the post

                            String val = gson.toJson(postSnapshot.getValue());

                            DTO_Estacionamiento estacionamiento = gson.fromJson(val,DTO_Estacionamiento.class);
                            lst.add(estacionamiento);
                            Log.d("FirebaseIO", val);

                        }


                        setMarkers(lst);







                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                }

        );

    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void updateColors(boolean animated) {
        int colorIndex = iconSwitch.getChecked().ordinal();
        toolbar.setBackgroundColor(toolbarColors[colorIndex]);

        if (animated) {
            switch (iconSwitch.getChecked()) {
                case LEFT:
                    statusBarAnimator.reverse();
                    title.setTextColor(getResources().getColor(R.color.colorAccent));
                    break;
                case RIGHT:
                    statusBarAnimator.start();
                    title.setTextColor(getResources().getColor(R.color.colorPrimary));
                    break;
            }
            revealToolbar();
        } else {
            window.setStatusBarColor(statusBarColors[colorIndex]);
        }
    }

    private void revealToolbar() {
        iconSwitch.getThumbCenter(revealCenter);
        moveFromSwitchToToolbarSpace(revealCenter);
        ViewAnimationUtils.createCircularReveal(toolbar,
                revealCenter.x, revealCenter.y,
                iconSwitch.getHeight(), toolbar.getWidth())
                .setDuration(DURATION_COLOR_CHANGE_MS)
                .start();
    }

    @Override
    public void onAnimationUpdate(ValueAnimator animator) {
        if (animator == statusBarAnimator) {
            int color = (Integer) animator.getAnimatedValue();
            window.setStatusBarColor(color);
        }
    }

    private void changeContentVisibility() {
        int targetTranslation = 0;
        Interpolator interpolator = null;
        switch (iconSwitch.getChecked()) {
            case LEFT:
                targetTranslation = 0;
                interpolator = contentInInterpolator;
                break;
            case RIGHT:
                targetTranslation = content.getHeight();
                interpolator = contentOutInterpolator;
                break;
        }
        content.animate().cancel();
        content.animate()
                .translationY(targetTranslation)
                .setInterpolator(interpolator)
                .setDuration(DURATION_COLOR_CHANGE_MS)
                .start();
    }

    @Override
    public void onCheckChanged(IconSwitch.Checked current) {
        updateColors(true);
        changeContentVisibility();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.credit_polyak:
//                open(URL_GITHUB_POLYAK);
//                break;
//            case R.id.credit_yarolegovich:
//                open(URL_GITHUB_YAROLEGOVICH);
//                break;
//            case R.id.credit_prokhoda:
//                open(URL_DRIBBBLE_PROKHODA);
//                break;
        }
    }

    private void open(Uri url) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(url);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Snackbar.make(content,
                    "asdasd",
                    Snackbar.LENGTH_SHORT)
                    .show();
        }
    }

    private void initAnimationRelatedFields() {
        revealCenter = new Point();
        statusBarAnimator = createArgbAnimator(
                statusBarColors[IconSwitch.Checked.LEFT.ordinal()],
                statusBarColors[IconSwitch.Checked.RIGHT.ordinal()]);
        contentInInterpolator = new OvershootInterpolator(0.5f);
        contentOutInterpolator = new DecelerateInterpolator();
    }

    private void initColors() {
        toolbarColors = new int[IconSwitch.Checked.values().length];
        statusBarColors = new int[toolbarColors.length];
        toolbarColors[IconSwitch.Checked.LEFT.ordinal()] = color(R.color.colorPrimary);
        statusBarColors[IconSwitch.Checked.LEFT.ordinal()] = color(R.color.colorPrimaryDark);
        toolbarColors[IconSwitch.Checked.RIGHT.ordinal()] = color(R.color.colorAccent);
        statusBarColors[IconSwitch.Checked.RIGHT.ordinal()] = color(R.color.colorAccent);
    }

    private ValueAnimator createArgbAnimator(int leftColor, int rightColor) {
        ValueAnimator animator = ValueAnimator.ofArgb(leftColor, rightColor);
        animator.setDuration(DURATION_COLOR_CHANGE_MS);
        animator.addUpdateListener(this);
        return animator;
    }

    private void moveFromSwitchToToolbarSpace(Point point) {
        point.set(point.x + iconSwitch.getLeft(), point.y + iconSwitch.getTop());
    }

    @ColorInt
    private int color(@ColorRes int res) {
        return ContextCompat.getColor(this, res);
    }


    public void setMarkers(List<DTO_Estacionamiento> lst)
    {

        googleMap.clear();



        List<Marker> lstMarkers = new ArrayList<>();
        for (DTO_Estacionamiento item : lst)
        {
            String[] lstC = item.cCoordenadas.split(",");
            LatLng Reporte = new LatLng(Double.parseDouble(lstC[0]), Double.parseDouble(lstC[1]));



//            MarkerOptions markerOption = new MarkerOptions().position(Reporte);
//
//            markerOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.uno));
//
//            markerOption.title("Precio" + ": $" + item.nPrecio + "0");
//            googleMap.addMarker(markerOption);



            Marker melbourne = googleMap.addMarker(new MarkerOptions()
                    .position(Reporte)
                    .title("Cajones" + ": " + item.nCajones)
                    .snippet("Precio" + ": $" + item.nPrecio + "0"));

            lstMarkers.add(melbourne);

            melbourne.showInfoWindow();


        }


        for (Marker item: lstMarkers) {

        }

        setItems(lst);



    }


    public void InicializarMapa(Bundle savedInstanceState, final Activity activity)
    {

        mMapView = (MapView) findViewById(R.id.mapita);
        mMapView.onCreate(savedInstanceState);


        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                Log.d("MapaFragment", "Se inicializo el mapa");
                // For showing a move to my location button
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    startInstalledAppDetailsActivity(activity);
                    return;
                }


                try {
                    // Customise the styling of the base map using a JSON object defined
                    // in a raw resource file.
                    boolean success = googleMap.setMapStyle(
                            MapStyleOptions.loadRawResourceStyle(
                                    getApplicationContext(), R.raw.style_json));

                    if (!success) {
                        Log.e(TAG, "Style parsing failed.");
                    }
                } catch (Resources.NotFoundException e) {
                    Log.e(TAG, "Can't find style. Error: ", e);
                }


                GPSTracker gps = new GPSTracker(getApplicationContext());
//                this.latitude = gps.getLatitude();
//                this.longitude = gps.getLongitude();

                LatLng sydney = new LatLng(gps.getLatitude(), gps.getLongitude());




                CameraUpdate center =
                        CameraUpdateFactory.newLatLng(sydney);
                CameraUpdate zoom = CameraUpdateFactory.zoomTo(14.0f);

                googleMap.moveCamera(center);
                googleMap.animateCamera(zoom);

                googleMap.setMyLocationEnabled(true);
                googleMap.getUiSettings().setMyLocationButtonEnabled(false);


            }
        });

    }

    public static void startInstalledAppDetailsActivity(final Activity context) {
        if (context == null) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }

    public void setItems(List<DTO_Estacionamiento> lst)
    {
        LinearLayoutManager lm = new LinearLayoutManager(this);
        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.recycler);
        recyclerView.setLayoutManager(lm);
        recyclerView.setAdapter(new Adaptador(this,lst));
        recyclerView.setNestedScrollingEnabled(false);

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("Request",requestCode+"");
        switch (requestCode) {
            case REQUEST_CODE_CAMARA:
                Log.d("Result",resultCode+"");
                if (resultCode == RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    String cQR = bundle.getString("cQR");
                    Log.d("RequestQR",cQR);
                    GenerarNota("123",cQR);
                }
                break;
        }
    }


    public void GenerarNota(String cPlacas, String nIdEstacionamiento)
    {

        pDialog.getProgressHelper().setBarColor(Color.parseColor("#ff5722"));
        pDialog.setTitleText("Iniciando");
        pDialog.setCancelable(false);
        pDialog.show();
        communicator.GenerarNotas(cPlacas, criptografo.decrypt(nIdEstacionamiento), this);
    }


    @Override
    public void onSuccess(DTO_Nota result) {


        pDialog.cancel();
        Snackbar.make(findViewById(R.id.contenedorcito), "Cuando te descocupes puedes generar tu nota", Snackbar.LENGTH_LONG).show();
        Log.d(TAG + "success",gson.toJson(result));

        fab2.setVisibility(View.VISIBLE);
        fab.setVisibility(View.GONE);



        ref.child("lstEstacionamientos").child("rosales").child("nCajones").setValue(result.nCajonesDisponibles);



        if(!result.bError)
        {
//            btn_generar.setVisibility(View.GONE);
//            btn_mostrar.setVisibility(View.VISIBLE);
//            cNotaCobro = result.getcQR();
            QRCodeWriter writer = new QRCodeWriter();
            try {
                BitMatrix bitMatrix = writer.encode(result.getcQR(), BarcodeFormat.QR_CODE, 512, 512);
                int width = bitMatrix.getWidth();
                int height = bitMatrix.getHeight();
                Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
                for (int x = 0; x < width; x++) {
                    for (int y = 0; y < height; y++) {
                        bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.parseColor("#fafafa"));
                    }
                }
                ((ImageView) findViewById(R.id.img_qr)).setImageBitmap(bmp);

            } catch (WriterException e) {
                e.printStackTrace();
            }
        }
        else
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Error");
            builder.setMessage("No se genero tu nota");
            AlertDialog alert1 = builder.create();
            alert1.show();
        }

    }

    @Override
    public void onFail(String result) {

    }



    @Override
    public void onWindowFocusChanged(boolean hasFocus)
    {
        if(hasFocus){

            if(bPantallaInicio)
            {
                bPantallaInicio = false;
                //mMapView.onResume();
                CerrarModal();
            }


        }
    }

    public void CerrarModal()
    {


        targetTranslation = modalDireccion.getHeight();
        interpolator = new DecelerateInterpolator();

        modalDireccion.animate().cancel();
        modalDireccion.animate()
                .translationY(targetTranslation)
                .setInterpolator(interpolator)
                .setDuration(DURATION_COLOR_CHANGE_MS)
                .start();
    }

    public void AbrirModal()
    {


        modalDireccion.setVisibility(View.VISIBLE);
        targetTranslation = 0;
        interpolator = new OvershootInterpolator(0.0f);

        modalDireccion.animate().cancel();
        modalDireccion.animate()
                .translationY(targetTranslation)
                .setInterpolator(interpolator)
                .setDuration(DURATION_COLOR_CHANGE_MS)
                .start();
    }







}

