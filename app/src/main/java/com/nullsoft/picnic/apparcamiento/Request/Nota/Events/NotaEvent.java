package com.nullsoft.picnic.apparcamiento.Request.Nota.Events;

import com.nullsoft.picnic.apparcamiento.Request.Nota.DTO_Nota;
import com.nullsoft.picnic.apparcamiento.Request.Nota.NotaCallback;

/**
 * Created by ruben on 12/02/17.
 */

public class NotaEvent {

    private DTO_Nota serverResponse;

    public NotaEvent(DTO_Nota serverResponse, final NotaCallback callback) {
        this.serverResponse = serverResponse;
        callback.onSuccess(serverResponse);
    }

    public DTO_Nota getServerResponse() {
        return serverResponse;
    }

    public void setServerResponse(DTO_Nota serverResponse) {
        this.serverResponse = serverResponse;
    }


}
