package com.nullsoft.picnic.apparcamiento.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.vision.text.Line;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.nullsoft.picnic.apparcamiento.DTO_Estacionamiento;
import com.nullsoft.picnic.apparcamiento.Helpers.Criptografo;
import com.nullsoft.picnic.apparcamiento.Helpers.GPSTracker;
import com.nullsoft.picnic.apparcamiento.R;
import com.nullsoft.picnic.apparcamiento.Request.Nota.CommunicatorNotas;
import com.nullsoft.picnic.apparcamiento.Request.Nota.DTO_Nota;
import com.nullsoft.picnic.apparcamiento.Request.Nota.NotaCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, ZXingScannerView.ResultHandler, NotaCallback {

    private static final String TAG = "MapsActivity";
    private GoogleMap mMap;
    public static final int CAPTURA_IMAGEN = 1000;
    private ZXingScannerView mScannerView;
    public Context mContext;

    public TextView cMensajeBoton;
    public LinearLayout btn_mostrar;
    public LinearLayout btn_generar;


    final Gson gson = new Gson();
    public Criptografo criptografo = new Criptografo("PQws");

    DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
    DatabaseReference mensajeRef = ref.child("lstEstacionamientos");

    //mapa
    MapView mMapView;
    private GoogleMap googleMap;
    double latitude;
    double longitude;

    public Button btn ;

    Bundle bundle;

    public CoordinatorLayout mRoot;

    public String cNotaCobro;

    final CommunicatorNotas communicator = new CommunicatorNotas();


    //variables modal
    public LinearLayout modalDireccion;
    public int tamanito = 0;
    public boolean bPantallaInicio = true;
    private static final int DURATION_COLOR_CHANGE_MS = 180;
    public int targetTranslation = 0;
    public Interpolator interpolator = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
//        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.map);
//        mapFragment.getMapAsync(this);

        bundle = savedInstanceState;
        InicializarMapa(savedInstanceState,this);

        mContext = this;

        cMensajeBoton = (TextView)findViewById(R.id.tv_mensajeboton);
        btn_generar = (LinearLayout)findViewById(R.id.btn_Generar);
        btn_mostrar = (LinearLayout)findViewById(R.id.btn_Mostrar);
        //btn = (Button) findViewById(R.id.btn_FotoCaptura);

        modalDireccion = (LinearLayout)findViewById(R.id.modalNota);
        modalDireccion.setVisibility(View.GONE);


        btn_mostrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AbrirModal();
            }
        });




        String cPassword = "12";

        Log.d(TAG,criptografo.encrypt(cPassword.getBytes()));



        mensajeRef.addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
//                        String val = gson.toJson(dataSnapshot.getValue());
//
//
//                        DTO_Estacionamiento sdf = gson.fromJson(val,DTO_Estacionamiento.class);
//
//
//                        //esta es la buena
//                        //List<String> td = (ArrayList<String>) dataSnapshot.getValue();
//
//                        //List<DTO_Estacionamiento> videos = gson.fromJson(val, new TypeToken<List<DTO_Estacionamiento>>(){}.getType());
//
//                        Log.d("FirebaseIO", gson.toJson(sdf));
//
//                        CambiarCajones(sdf.getnCajones() +"", sdf.getcCoordenadas());


                        List<DTO_Estacionamiento> lst = new ArrayList<>();
                        for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                            // TODO: handle the post

                            String val = gson.toJson(postSnapshot.getValue());

                            DTO_Estacionamiento estacionamiento = gson.fromJson(val,DTO_Estacionamiento.class);
                            lst.add(estacionamiento);
                            Log.d("FirebaseIO", val);

                        }


                        setMarkers(lst);





                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                }

        );




    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        GPSTracker gps = new GPSTracker(getApplicationContext());


        LatLng sydney = new LatLng(gps.getLatitude(), gps.getLongitude());
//                mMap.addMarker(new MarkerOptions().position(sydney).title("Tu estas aqui"));
//                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney,18.0f));

        CameraUpdate center =
                CameraUpdateFactory.newLatLng(sydney);
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(18.0f);

        mMap.moveCamera(center);
        mMap.animateCamera(zoom);

        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);

        Log.d(TAG,gps.getLatitude() + " - " + gps.getLongitude());

        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.style_json));

            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }


    }


    public void QrScanner(View view){


        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        setContentView(mScannerView);

        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();         // Start camera

    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here

        //mScannerView.stopCamera();


        LayoutInflater inflater= this.getLayoutInflater();

        View layout=  inflater.inflate(R.layout.activity_maps,null);
        setContentView(layout);

        InicializarMapa(bundle,this);
        modalDireccion = (LinearLayout)findViewById(R.id.modalNota);
        btn_generar = (LinearLayout)findViewById(R.id.btn_Generar);
        btn_mostrar = (LinearLayout)findViewById(R.id.btn_Mostrar);
        modalDireccion.setVisibility(View.GONE);
        CerrarModal();

        ImageView btn_Cerrar = (ImageView)findViewById(R.id.btn_Cerrar);
        btn_Cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CerrarModal();
            }
        });
        btn_mostrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AbrirModal();
            }
        });


        mRoot = (CoordinatorLayout) findViewById(R.id.main_content);



        Log.e("handler", rawResult.getText()); // Prints scan results
        Log.e("handler", rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode)


        String resultQR = criptografo.decrypt(rawResult.getText());

        GenerarNota("123",resultQR);
        // show the scanner result into dialog box.


        // If you would like to resume scanning, call this method below:
        // mScannerView.resumeCameraPreview(this);
    }



    public void InicializarMapa(Bundle savedInstanceState, final Activity activity)
    {

        mMapView = (MapView) findViewById(R.id.mapita);
        mMapView.onCreate(savedInstanceState);
        GPSTracker gps = new GPSTracker(getApplicationContext());
        this.latitude = gps.getLatitude();
        this.longitude = gps.getLongitude();

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                Log.d("MapaFragment", "Se inicializo el mapa");
                // For showing a move to my location button
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    startInstalledAppDetailsActivity(activity);
                    return;
                }


                try {
                    // Customise the styling of the base map using a JSON object defined
                    // in a raw resource file.
                    boolean success = googleMap.setMapStyle(
                            MapStyleOptions.loadRawResourceStyle(
                                    getApplicationContext(), R.raw.style_json));

                    if (!success) {
                        Log.e(TAG, "Style parsing failed.");
                    }
                } catch (Resources.NotFoundException e) {
                    Log.e(TAG, "Can't find style. Error: ", e);
                }


                // For dropping a marker at a point on the Map
                //LatLng sydney = new LatLng(24.811168457401543,-107.46831133961678);

                // For zooming automatically to the location of the marker
                //CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(12).build();
                //googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


                //googleMap.setMyLocationEnabled(true);
                googleMap.getUiSettings().setMyLocationButtonEnabled(false);

                LatLng sydney = new LatLng(latitude, longitude);


//                MarkerOptions markerOption = new MarkerOptions().position(sydney);
//
//                markerOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.));
//
//                markerOption.title("Casa");
//                markerOption.draggable(true);

                //googleMap.addMarker(markerOption);



                CameraUpdate center =
                        CameraUpdateFactory.newLatLng(sydney);
                CameraUpdate zoom = CameraUpdateFactory.zoomTo(18.0f);

                googleMap.moveCamera(center);
                googleMap.animateCamera(zoom);



                googleMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                    @Override
                    public void onMarkerDragStart(Marker arg0) {
                        // TODO Auto-generated method stub
                        Log.d("System out", "onMarkerDragStart..."+arg0.getPosition().latitude+"..."+arg0.getPosition().longitude);
                    }

                    @SuppressWarnings("unchecked")
                    @Override
                    public void onMarkerDragEnd(Marker arg0) {
                        // TODO Auto-generated method stub
                        Log.d("System out", "onMarkerDragEnd..."+arg0.getPosition().latitude+"..."+arg0.getPosition().longitude);

                        googleMap.animateCamera(CameraUpdateFactory.newLatLng(arg0.getPosition()));
                    }

                    @Override
                    public void onMarkerDrag(Marker arg0) {
                        // TODO Auto-generated method stub
                        Log.i("System out", "onMarkerDrag...");
                    }
                });



                AgregarEstacionamiento("asd");
            }
        });

    }

    public static void startInstalledAppDetailsActivity(final Activity context) {
        if (context == null) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }

    public void CambiarCajones(String Cajones,String cCoordenadas)
    {



        //Toast.makeText(mContext,Cajones,Toast.LENGTH_SHORT);
//        btn.setText(Cajones);
//
//        btn.getRootView();

        Snackbar.make(findViewById(R.id.main_content), "Cajones: " + Cajones, Snackbar.LENGTH_LONG)
                .show();


        googleMap.clear();

        if(cCoordenadas != "")
        {

            String[] lst = cCoordenadas.split(",");
            Log.d("lat long", lst[0] + " - " + lst[1]);
            LatLng Reporte = new LatLng(Double.parseDouble(lst[0]), Double.parseDouble(lst[1]));




            //markerOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.markercustom));

            //markerOption.title("Cajones" + ": " + Cajones);
            //markerOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.mr_ic_audiotrack_light));

            Marker melbourne = googleMap.addMarker(new MarkerOptions()
                    .position(Reporte)
                    .title("Cajones" + ": " + Cajones)
                    .snippet("Cajones" + ": " + Cajones));

            melbourne.showInfoWindow();



            //googleMap.addMarker(melbourne);






        }
    }


    public void AgregarEstacionamiento(String cCoordenadas)
    {
        //Log.d("Estacionamientos",mensajeRef.getDatabase().toString());

    }

    public void GenerarNota(String cPlacas, String nIdEstacionamiento)
    {
        communicator.GenerarNotas(cPlacas, nIdEstacionamiento, this);
    }


    @Override
    public void onSuccess(DTO_Nota result) {

        Log.d(TAG + "success",gson.toJson(result));



        ref.child("lstEstacionamientos").child("rosales").child("nCajones").setValue(result.nCajonesDisponibles);



        if(!result.bError)
        {
            btn_generar.setVisibility(View.GONE);
            btn_mostrar.setVisibility(View.VISIBLE);
            cNotaCobro = result.getcQR();
            QRCodeWriter writer = new QRCodeWriter();
            try {
                BitMatrix bitMatrix = writer.encode(result.getcQR(), BarcodeFormat.QR_CODE, 512, 512);
                int width = bitMatrix.getWidth();
                int height = bitMatrix.getHeight();
                Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
                for (int x = 0; x < width; x++) {
                    for (int y = 0; y < height; y++) {
                        bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.parseColor("#fafafa"));
                    }
                }
                ((ImageView) findViewById(R.id.img_qr)).setImageBitmap(bmp);

            } catch (WriterException e) {
                e.printStackTrace();
            }
        }
        else
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Error");
            builder.setMessage("No se genero tu nota");
            AlertDialog alert1 = builder.create();
            alert1.show();
        }

//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setTitle("Scan Result");
//        builder.setMessage(result.getcQR());
//        AlertDialog alert1 = builder.create();
//        alert1.show();
    }

    @Override
    public void onFail(String result) {

    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus)
    {
        if(hasFocus){

            if(bPantallaInicio)
            {
                bPantallaInicio = false;
                mMapView.onResume();
                CerrarModal();
            }


        }
    }

    public void CerrarModal()
    {


        targetTranslation = modalDireccion.getHeight();
        interpolator = new DecelerateInterpolator();

        modalDireccion.animate().cancel();
        modalDireccion.animate()
                .translationY(targetTranslation)
                .setInterpolator(interpolator)
                .setDuration(DURATION_COLOR_CHANGE_MS)
                .start();
    }

    public void AbrirModal()
    {


        modalDireccion.setVisibility(View.VISIBLE);
        targetTranslation = 0;
        interpolator = new OvershootInterpolator(0.0f);

        modalDireccion.animate().cancel();
        modalDireccion.animate()
                .translationY(targetTranslation)
                .setInterpolator(interpolator)
                .setDuration(DURATION_COLOR_CHANGE_MS)
                .start();
    }


    public void setMarkers(List<DTO_Estacionamiento> lst)
    {
        googleMap.clear();



        List<Marker> lstMarkers = new ArrayList<>();
        for (DTO_Estacionamiento item : lst)
        {
            String[] lstC = item.cCoordenadas.split(",");
            LatLng Reporte = new LatLng(Double.parseDouble(lstC[0]), Double.parseDouble(lstC[1]));




            //markerOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.markercustom));

            //markerOption.title("Cajones" + ": " + Cajones);
            //markerOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.mr_ic_audiotrack_light));

            Marker melbourne = googleMap.addMarker(new MarkerOptions()
                    .position(Reporte)
                    .title("Cajones" + ": " + item.nCajones)
                    .snippet("Precio" + ": " + item.nPrecio));

            lstMarkers.add(melbourne);

            melbourne.showInfoWindow();


        }


        for (Marker item: lstMarkers) {

        }



    }
}
