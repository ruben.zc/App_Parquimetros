package com.nullsoft.picnic.apparcamiento.Request.Nota;

/**
 * Created by Theodhor Pandeli on 2/11/2016.
 */

import com.squareup.otto.Bus;

public class BusProvider {

    private static final Bus BUS = new Bus();

    public static Bus getInstance() {
        return BUS;
    }

    public BusProvider() {
    }
}

