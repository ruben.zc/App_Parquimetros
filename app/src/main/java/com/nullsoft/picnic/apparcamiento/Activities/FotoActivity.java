package com.nullsoft.picnic.apparcamiento.Activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.nullsoft.picnic.apparcamiento.Helpers.Criptografo;
import com.nullsoft.picnic.apparcamiento.R;
import com.nullsoft.picnic.apparcamiento.Request.Nota.CommunicatorNotas;
import com.nullsoft.picnic.apparcamiento.Request.Nota.DTO_Nota;
import com.nullsoft.picnic.apparcamiento.Request.Nota.NotaCallback;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class FotoActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler, NotaCallback {


    private ZXingScannerView mScannerView;
    final CommunicatorNotas communicator = new CommunicatorNotas();
    public Criptografo criptografo = new Criptografo("PQws");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foto);


        LayoutInflater inflater= this.getLayoutInflater();

        View layout=  inflater.inflate(R.layout.activity_maps,null);

        QrScanner(layout);

    }


    public void QrScanner(View view){



        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        setContentView(mScannerView);

        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();         // Start camera

    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result rawResult) {



        Log.e("handler", rawResult.getText()); // Prints scan results
        Log.e("handler", rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode)


        //String resultQR = criptografo.decrypt(rawResult.getText());

        Bundle bundle= new Bundle();
        bundle.putString("cQR",rawResult.getText());

        Intent returnIntent = new Intent();
        returnIntent.putExtras(bundle);

        setResult(LoginActivity.RESULT_OK, returnIntent);
        finish();



        //GenerarNota("123",resultQR);
        // show the scanner result into dialog box.


        // If you would like to resume scanning, call this method below:
        // mScannerView.resumeCameraPreview(this);
    }

    public void GenerarNota(String cPlacas, String nIdEstacionamiento)
    {
        communicator.GenerarNotas(cPlacas, nIdEstacionamiento, this);
    }


    @Override
    public void onSuccess(DTO_Nota result) {

        //Log.d(TAG + "success",gson.toJson(result));



        //ref.child("lstEstacionamientos").child("rosales").child("nCajones").setValue(result.nCajonesDisponibles);



        if(!result.bError)
        {
            QRCodeWriter writer = new QRCodeWriter();
            try {
                BitMatrix bitMatrix = writer.encode(result.getcQR(), BarcodeFormat.QR_CODE, 512, 512);
                int width = bitMatrix.getWidth();
                int height = bitMatrix.getHeight();
                Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
                for (int x = 0; x < width; x++) {
                    for (int y = 0; y < height; y++) {
                        bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.parseColor("#fafafa"));
                    }
                }
                ((ImageView) findViewById(R.id.img_qr)).setImageBitmap(bmp);



            } catch (WriterException e) {
                e.printStackTrace();
            }
        }
        else
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Error");
            builder.setMessage("No se genero tu nota");
            AlertDialog alert1 = builder.create();
            alert1.show();
        }


    }

    @Override
    public void onFail(String result) {

    }

}
