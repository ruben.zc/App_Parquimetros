package com.nullsoft.picnic.apparcamiento.Request.Nota;

/**
 * Created by rubenzamorano on 03/06/17.
 */

public class DTO_Nota {
    public String cMensaje;
    public boolean bError;
    public String cQR;
    public int nCajonesDisponibles;

    public DTO_Nota(String cMensaje, boolean bError, String cQR, int nCajonesDisponibles) {
        this.cMensaje = cMensaje;
        this.bError = bError;
        this.cQR = cQR;
        this.nCajonesDisponibles = nCajonesDisponibles;
    }

    public String getcMensaje() {
        return cMensaje;
    }

    public void setcMensaje(String cMensaje) {
        this.cMensaje = cMensaje;
    }

    public boolean isbError() {
        return bError;
    }

    public void setbError(boolean bError) {
        this.bError = bError;
    }

    public String getcQR() {
        return cQR;
    }

    public void setcQR(String cQR) {
        this.cQR = cQR;
    }

    public int getnCajonesDisponibles() {
        return nCajonesDisponibles;
    }

    public void setnCajonesDisponibles(int nCajonesDisponibles) {
        this.nCajonesDisponibles = nCajonesDisponibles;
    }
}
