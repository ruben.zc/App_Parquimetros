package com.nullsoft.picnic.apparcamiento.Request.Nota.Events;


import com.nullsoft.picnic.apparcamiento.Request.Nota.NotaCallback;

/**
 * Created by Theodhor Pandeli on 2/11/2016.
 */
public class ErrorEvent {
    private int errorCode;
    private String errorMsg;

    public ErrorEvent(int errorCode, String errorMsg, final NotaCallback callback) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
        callback.onFail(this.errorMsg);
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
