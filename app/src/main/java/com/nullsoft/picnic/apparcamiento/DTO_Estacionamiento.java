package com.nullsoft.picnic.apparcamiento;

/**
 * Created by ruben on 01/06/17.
 */

public class DTO_Estacionamiento {

    public String cNombre;
    public int nId;
    public String cCoordenadas;
    public double nPrecio;
    public int nCajones;

    public DTO_Estacionamiento(String cNombre, int nId, String cCoordenadas, double nPrecio, int nCajones) {
        this.cNombre = cNombre;
        this.nId = nId;
        this.cCoordenadas = cCoordenadas;
        this.nPrecio = nPrecio;
        this.nCajones = nCajones;
    }

    public String getcNombre() {
        return cNombre;
    }

    public void setcNombre(String cNombre) {
        this.cNombre = cNombre;
    }

    public int getnId() {
        return nId;
    }

    public void setnId(int nId) {
        this.nId = nId;
    }

    public String getcCoordenadas() {
        return cCoordenadas;
    }

    public void setcCoordenadas(String cCoordenadas) {
        this.cCoordenadas = cCoordenadas;
    }

    public double getnPrecio() {
        return nPrecio;
    }

    public void setnPrecio(double nPrecio) {
        this.nPrecio = nPrecio;
    }

    public int getnCajones() {
        return nCajones;
    }

    public void setnCajones(int nCajones) {
        this.nCajones = nCajones;
    }
}
