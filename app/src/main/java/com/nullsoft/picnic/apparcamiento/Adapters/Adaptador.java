package com.nullsoft.picnic.apparcamiento.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.nullsoft.picnic.apparcamiento.DTO_Estacionamiento;
import com.nullsoft.picnic.apparcamiento.R;

import java.util.List;

//import com.bumptech.glide.Glide;

/**
 * Adaptador para mostrar las comidas más pedidas en la sección "Inicio"
 */
public class Adaptador extends RecyclerView.Adapter<Adaptador.ViewHolder> {


    public static class ViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        public TextView cNombre;
        public TextView cDireccion;
        public TextView cCajones;
        public TextView cTarifa;
        public ImageView imagen;
        //public CardView cardView;

        public ViewHolder(View v) {
            super(v);
            cNombre = (TextView) v.findViewById(R.id.cNombreEstacionamiento);
            cDireccion = (TextView) v.findViewById(R.id.cDireccion);
            cCajones = (TextView) v.findViewById(R.id.cCajones);
            cTarifa = (TextView) v.findViewById(R.id.cTarifa);
        }
    }

    private Context mContext;
    private Context mContextactivity;
    public List<DTO_Estacionamiento> items;

    public Adaptador(Context context, List<DTO_Estacionamiento> lst)
    {
        mContext = context;
        items = lst;

    }



    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item, viewGroup, false);

    mContextactivity = viewGroup.getContext();
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        DTO_Estacionamiento item = items.get(i);

        viewHolder.cNombre.setText(item.getcNombre());
        viewHolder.cDireccion.setText("Cerca del centro");
        viewHolder.cTarifa.setText("$" + item.getnPrecio() + "0");
        viewHolder.cCajones.setText(item.getnCajones() + " Cajones");


    }



}