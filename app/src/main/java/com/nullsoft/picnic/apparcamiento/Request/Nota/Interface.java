package com.nullsoft.picnic.apparcamiento.Request.Nota;


import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Dori on 12/28/2016.
 */

public interface Interface {

    @FormUrlEncoded
    @POST("/PQ.asmx/GenerarNota")
    Call<DTO_Nota> post(
            @Field("cPlacas") String cPlacas,
            @Field("nIdEstacionamiento") String nIdEstacionamiento
    );

    @GET("/sp/index.php")
    Call<DTO_Nota> get(
            @Query("method") String method,
            @Query("username") String username,
            @Query("password") String password
    );

}