package com.nullsoft.picnic.apparcamiento.Request.Nota;

import com.google.gson.Gson;
import com.nullsoft.picnic.apparcamiento.Request.Nota.Events.ErrorEvent;
import com.nullsoft.picnic.apparcamiento.Request.Nota.Events.NotaEvent;
import com.squareup.otto.Produce;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Dori on 12/28/2016.
 */

public class CommunicatorNotas {
    private static final String TAG = "Communicator";
    private static final String SERVER_URL = "http://tkcore.ddns.net:91";

    final Gson gson = new Gson();


    public void GenerarNotas(String cPlacas, String nIdEstacionamiento, final NotaCallback callback) {


        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(SERVER_URL)
                .build();

        Interface service = retrofit.create(Interface.class);

        Call<DTO_Nota> call = service.post(cPlacas, nIdEstacionamiento);

        call.enqueue(new Callback<DTO_Nota>() {
            @Override
            public void onResponse(Call<DTO_Nota> call, Response<DTO_Nota> response) {
                // response.isSuccessful() is true if the response code is 2xx
                BusProvider.getInstance().post(new NotaEvent(response.body(), callback));
            }

            @Override
            public void onFailure(Call<DTO_Nota> call, Throwable t) {
                // handle execution failures like no internet connectivity
                BusProvider.getInstance().post(new ErrorEvent(-2, t.getMessage(),callback));
            }
        });
    }


    @Produce
    public NotaEvent produceServerEvent(DTO_Nota serverResponse, NotaCallback callback) {
        return new NotaEvent(serverResponse, callback);
    }

    @Produce
    public ErrorEvent produceErrorEvent(int errorCode, String errorMsg,NotaCallback callback) {
        return new ErrorEvent(errorCode, errorMsg, callback);
    }
}
